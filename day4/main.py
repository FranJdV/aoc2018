import datetime
from collections import Counter
import re


def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def prepare_data(f: str) -> list:
    return sorted(list([datetime.datetime.strptime(e[0], "[%Y-%m-%d %H:%M"), e[1]] for e in [x.strip("\n").split("]") for x in read_input(f)]))


def find_guard_most_asleep(l: list) -> str:
    guard_nb = ""
    time_falls_asleep = datetime.datetime.now()
    cnt = Counter()
    for i in l:
        if re.match(" Guard", i[1]):
            guard_nb = i[1].split(" ")[2].strip("#")
        if re.match(" falls", i[1]):
            time_falls_asleep = i[0]
        if re.match(" wakes", i[1]):
            cnt[guard_nb] += ((i[0]-time_falls_asleep).seconds/60)
    return cnt.most_common(1)[0][0]


def part1(l: list) -> int:
    guard_most_asleep = find_guard_most_asleep(l)
    time_falls_asleep = 0
    cnt = Counter()
    guard_nb = ""
    for i in l:
        if re.match(" Guard", i[1]):
            guard_nb = i[1].split(" ")[2].strip("#")
        if re.match(" falls", i[1]):
            time_falls_asleep = i[0].minute
        if re.match(" wakes", i[1]) and guard_nb == guard_most_asleep:
                cnt += Counter(x for x in range(time_falls_asleep, i[0].minute))
    return int(guard_most_asleep)*int(cnt.most_common(1)[0][0])


def part2(l: list) -> int:
    guard_minutes = {}
    guard_nb = ""
    time_falls_asleep = 0
    for i in l:
        if re.match(" Guard", i[1]):
            guard_nb = i[1].split(" ")[2].strip("#")
        if re.match(" falls", i[1]):
            time_falls_asleep = i[0].minute
        if re.match(" wakes", i[1]):
            for j in range(time_falls_asleep, i[0].minute):
                if (guard_nb, j) in guard_minutes:
                    guard_minutes[(guard_nb, j)] += 1
                else:
                    guard_minutes.update({(guard_nb, j): 1})
    result = max(guard_minutes, key=guard_minutes.get)
    return int(result[0])*result[1]


if __name__ == '__main__':
    input_file_name = "input.txt"
    list_input = prepare_data(input_file_name)
    print(f'result part 1 = {part1(list_input)}')
    print(f'result part 2 = {part2(list_input)}')
