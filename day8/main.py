def read_input(f: str) -> str:
    with open(f, "r") as input_file:
        return input_file.readline()


def read_node(index: int, l: list, list_metadata: list) -> int:
    test = index + 2
    for i in range(0, l[index]):
        test = read_node(test, l, list_metadata)
    list_metadata.extend(l[test: test + l[index + 1]])
    return test + l[index + 1]


def part1(l: list) -> int:
    list_metadata = list()
    read_node(0, l, list_metadata)
    return sum(list_metadata)


def calculate_node(index: int, l: list) -> (int, int):
    node = dict()
    test = index + 2
    for i in range(0, l[index]):
        node[i+1], test = calculate_node(test, l)

    return sum(l[test: test + l[index + 1]]) if l[index] == 0 else sum(node[l[j]] for j in range(test, test + l[index + 1]) if l[j] in node.keys()), test + l[index + 1]


def part2(l: list) -> int:
    value, i = calculate_node(0, l)
    return value


if __name__ == '__main__':
    input_file_name = "input.txt"
    input_list = [int(x) for x in read_input(input_file_name).split(" ")]
    print(f'result part 1 = {part1(input_list)}')
    print(f'result part 2 = {part2(input_list)}')
