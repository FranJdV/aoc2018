import numpy as np


def calculate_power_level(x: int, y: int) -> int:
    grid_serial_number = 9435
    return int(str(((x + 11)*(y+1) + grid_serial_number)*(x + 11))[-3]) - 5


def part1() -> (int, int):
#    grid = np.fromfunction(calculate_power_level, (10, 10), dtype=int)
    grid = np.zeros((300, 300), dtype=int)
    for i in range(300):
        for j in range(300):
            grid[i, j] = calculate_power_level(i, j)
    test = {(x+1, y+1): grid[x:x+3, y:y+3].sum() for x in range(297) for y in range(297)}
    return max(test.items(), key=lambda kv:  kv[1])[0]


def part2() -> (int, int, int):
    grid = np.zeros((300, 300), dtype=int)
    for i in range(300):
        for j in range(300):
            grid[i, j] = calculate_power_level(i, j)
    test = {(x+1, y+1, z): grid[x:x+z, y:y+z].sum() for x in range(300) for y in range(300) for z in range(1, 300) if (x+z <= 300 and y+z <= 300)}
    return max(test.items(), key=lambda kv:  kv[1])[0]


if __name__ == '__main__':
    np.set_printoptions(threshold=np.nan)
    print(f'result part 1 = {part1()}')
    print(f'result part 2 = {part2()}')
