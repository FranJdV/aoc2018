import re
import numpy as np


def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def print_output(list_points: list):
    max_x = max([abs(x[0]) for x in list_points]) * 2 + 2
    max_y = max([abs(x[1]) for x in list_points]) * 2 + 2
    min_x = int(max_x/2 + min([x[0] for x in list_points]))
    min_y = int(max_y/2 + min([x[1] for x in list_points]))

    test = np.zeros(shape=(max_x - min_x, max_y - min_y), dtype=int)
    for i in list_points:
        test[int(max_x/2 + i[0]) - min_x][int(max_y/2 + i[1]) - min_y] = 1
    for j in range(max_y - min_y):
        print("".join(["." if test[k][j] == 0 else "#" for k in range(max_x - min_x)]))


def part1(list_points: list, limit: int) -> int:
    for i in range(10000):
        list_points = [(x[0]+x[2], x[1]+x[3], x[2], x[3]) for x in list_points]

    for j in range(limit):
        print(j)
        print_output(list_points)
        list_points = [(x[0]+x[2], x[1]+x[3], x[2], x[3]) for x in list_points]
        print("\n")
    return 0


def part2() -> int:
    return 0


if __name__ == '__main__':
    input_file_name = "input.txt"
    pattern = re.compile('position=<([\s\-0-9]*), ([\s\-0-9]*)> velocity=<([<\s\-0-9]*), ([\s\-0-9]*)>\n')
    input_list = [(int(x.groups()[0]), int(x.groups()[1]), int(x.groups()[2]), int(x.groups()[3])) for x in map(pattern.match,  read_input(input_file_name))]
    print(f'result part 1 = {part1(input_list, 10)}')
    print(f'result part 2 = {part2()}')
