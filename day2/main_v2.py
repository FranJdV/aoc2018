from collections import Counter
import itertools


def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def part1(ids: list) -> int:
    two_times = 0
    three_times = 0
    for i in ids:
        cnt = Counter(l for l in i)
        if len(list(itertools.filterfalse(lambda x: x != 2, cnt.values()))) >= 1:
            two_times += 1
        if len(list(itertools.filterfalse(lambda y: y != 3, cnt.values()))) >= 1:
            three_times += 1
    return two_times*three_times


def part1_v2(ids: list) -> int:
    two_times = 0
    three_times = 0
    for i in ids:
        cnt = Counter(l for l in i)
        if len((list(x for x in cnt.values() if x == 2))) >= 1:
            two_times += 1
        if len((list(x for x in cnt.values() if x == 3))) >= 1:
            three_times += 1
    return two_times*three_times


def part2(ids: list) -> str:
    for first in ids:
        for second in ids:
            if len(list((x for x in range(len(first)) if first[x] != second[x]))) == 1:
                return "".join((first[i] for i in range(len(first)) if first[i] == second[i]))


if __name__ == '__main__':
    input_file_name = "input.txt"
#    list_id = list(map(str.strip, read_input(input_file_name)))
    list_id = list(x.strip("\n") for x in read_input(input_file_name))
    print(f'result part 1 = {part1(list_id)}')
    print(f'result part 1 = {part1_v2(list_id)}')
    print(f'result part 2 = {part2(list_id)}')
