def read_input(f: str) -> list:
    input_list = []
    with open(f, "r") as input_file:
        for line in input_file:
            input_list.append(line)

    return input_list


def part1(ids: list):
    two_times = 0
    three_times = 0
    for i in ids:
        letters = dict()
        for l in i:
            if l in letters:
                letters[l] += 1
            else:
                letters[l] = 1

        two_times_found = False
        three_times_found = False
        for e in letters:
            if letters[e] == 2 and not two_times_found:
                two_times += 1
                two_times_found = True
            if letters[e] == 3 and not three_times_found:
                three_times += 1
                three_times_found = True

    print(two_times)
    print(three_times)
    result = two_times*three_times
    print("result : part 1" + str(result))


def part2(ids: list):
    first_id = ""
    second_id = ""
    for first in ids:
        for second in ids:
            nb_diff = 0
            for i in range(0, 26):
                if first[i] != second[i]:
                    nb_diff += 1
            if nb_diff == 1:
                first_id = first
                second_id = second
                break

    print(first_id)
    print(second_id)

    result = ""
    for l in range(0, 26):
        if first_id[l] == second_id[l]:
            result += first_id[l]

    print("result part 2 : " + result)


def main():
    input_file_name = "input.txt"
    list_id = read_input(input_file_name)
    part1(list_id)
    part2(list_id)


main()
