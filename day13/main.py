import numpy as np


CART_SYMBOL = {">": "-", "<": "-", "^": "|", "v": "|"}
CART_MOVE = {">": (0, 1), "<": (0, -1), "^": (-1, 0), "v": (1, 0)}


def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def print_result(grid: np.array, carts: dict, x: int, y: int) -> None:
    print_grid = np.array(grid)
    for m in range(x):
        for n in range(y):
            if (m, n) in carts:
                print_grid[m, n] = carts[m, n][0]
    for b in range(x):
        print("".join(print_grid[b, :]))


def move(grid: np.array, carts: dict, x: int, y: int) -> dict:
    new_carts = {}
    for m in range(x):
        for n in range(y):
            if (m, n) in carts:
                next_m = m + CART_MOVE[carts[m, n][0]][0]
                next_n = n + CART_MOVE[carts[m, n][0]][1]
                if (next_m, next_n) in carts or (next_m, next_n) in new_carts:
                    return next_m, next_n
                else:
                    # TODO if \ or / or +, le carts[0] change de sens + carts[1] aussi si +
                    new_carts[next_m, next_n] = carts[m, n]
                    del carts[m, n]
    return new_carts


def part1(l: list) -> (int, int):
    grid = np.empty(shape=(len(l), len(l[0])), dtype=str)
    carts = {}
    for i in range(len(l)):
        for j in range(len(l[i])):
            if l[i][j] in CART_SYMBOL.keys():
                carts[i, j] = (l[i][j], 0)
                grid[i, j] = CART_SYMBOL[l[i][j]]
            else:
                grid[i, j] = l[i][j]
    print(carts)
    print_result(grid, carts, len(l), len(l[0]))

    for x in range(2):
        carts = move(grid, carts, len(l), len(l[0]))
        print(carts)
        print_result(grid, carts, len(l), len(l[0]))
    return 0, 0


def part2() -> int:
    return 0


if __name__ == '__main__':
    np.set_printoptions(threshold=np.nan)
    input_file_name = "input2.txt"
    input_list = [x.strip("\n") for x in read_input(input_file_name)]
    print(input_list)
    print(f'result part 1 = {part1(input_list)}')
    print(f'result part 2 = {part2()}')
