import sys


def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def look_for_next_step(next_step: list, result: list, list_before: list, list_after: list) -> list:
    if len(next_step) == 0:
        return result
    i = 0
    while next_step[i] in list_before and len(set(list_before[next_step[i]]) & set(result)) != len(list_before[next_step[i]]):
        i += 1
    e = next_step[i]
    result.append(e)
    next_step.remove(e)

    if e in list_after:
        next_step.extend(list_after[e])
    next_step = sorted(set(next_step))
    return look_for_next_step(next_step, result, list_before, list_after)


def part1(l: list) -> str:

    list_before = {}
    for e in l:
        if e[1] not in list_before.keys():
            list_before[e[1]] = list(e[0])
        else:
            list_before[e[1]].append(e[0])

    list_after = {}
    for e in l:
        if e[0] not in list_after.keys():
            list_after[e[0]] = list(e[1])
        else:
            list_after[e[0]].append(e[1])

    #next = sorted(list(itertools.filterfalse(lambda x: x in list_before.keys(), list_after.keys())))

    result = look_for_next_step(sorted(set(list_before) ^ set(list_after)),list(), list_before, list_after)

    return "".join(result)


def work(next_step: list, result : int, workers: list, done: list, list_before: list, list_after: list) -> int:
    for f in range(len(workers)):
        if workers[f][0] != "." and workers[f][1] == 0:
            done.append(workers[f][0])
            workers[f] = (".", 0)
    for a in range(len([w[0] for w in workers if w[1] == 0])):
        i = 0
        while i < len(next_step) and next_step[i] in list_before and len(set(list_before[next_step[i]]) & set(done)) != len(list_before[next_step[i]]):
            i += 1
        if i < len(next_step):
            e = next_step[i]
            for j in range(len(workers)):
                if workers[j][0] == ".":
                    workers[j] = (e, 60 + (ord(e) - ord('A'))+1)
                    break
            next_step.remove(e)

            if e in list_after:
                next_step.extend(list_after[e])
            next_step = sorted(set(next_step))
    result += 1
    workers = list((x[0], x[1]-1 if x[1] != 0 else 0) for x in workers)
    if len(next_step) == 0 and len([w[0] for w in workers if w[1] == 0]) == len(workers):
        return result
    else:
        return work(next_step, result, workers, done, list_before, list_after)


def part2(l: list) -> int:
    list_before = {}
    for e in l:
        if e[1] not in list_before.keys():
            list_before[e[1]] = list(e[0])
        else:
            list_before[e[1]].append(e[0])

    list_after = {}
    for e in l:
        if e[0] not in list_after.keys():
            list_after[e[0]] = list(e[1])
        else:
            list_after[e[0]].append(e[1])

#Python limit recursion is under 1000, our result is 1040
    sys.setrecursionlimit(1100)
    return work(sorted(set(list_before) ^ set(list_after)), 0, [(".", 0)]*5, list(), list_before, list_after)


if __name__ == '__main__':
    input_file_name = "input.txt"
    input_list = list([x.split(" ")[1], x.split(" ")[7]] for x in read_input(input_file_name))
    print(f'result part 1 = {part1(input_list)}')
    print(f'result part 2 = {part2(input_list)}')
