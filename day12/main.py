
def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def calc(nb_generation: int, initial_grid: str, notes: dict) ->(str, int):
    grid = "...." + initial_grid + "...."
    index_position = -4
    for h in range(nb_generation):
        index_first = grid.index("#")
        grid = "".join(["." if grid[i-2:i+3] not in notes else notes[grid[i-2:i+3]] for i in range(index_first - 2, len(grid) - 3)])
        grid = "...." + grid + "...."
        index_position += (index_first - 2) - 4
    return grid, index_position


def find_occurrences(nb_generation: int, initial_grid: str, notes: dict):
    list_grid = [initial_grid]
    grid = "...." + initial_grid + "...."
    index_position = -4
    for h in range(nb_generation):
        index_first = grid.index("#")
        grid = "".join(["." if grid[i-2:i+3] not in notes else notes[grid[i-2:i+3]] for i in range(index_first - 2, len(grid) - 3)])
        index_position += (index_first - 2) - 4

        test_grid = [x for x in grid]
        test_grid.reverse()
        test = test_grid.index("#")
        grid_to_save = grid[grid.index("#"):len(grid) - test]
        if grid_to_save in list_grid:
            print(grid_to_save)
            print(h)
            print(index_position)
        list_grid.append(grid_to_save)

        grid = "...." + grid + "...."
    return


def part1(initial_grid: str, notes: dict) -> int:
    grid, index = calc(20, initial_grid, notes)
    return sum([j + index for j in range(len(grid)) if grid[j] == "#"])


def part2(initial_grid: str, notes: dict) -> int:
    #find_occurrences(150, initial_grid, notes)
    #Always the same result after 129 occurrences, just +1 to index_position
    grid, index = calc(130, initial_grid, notes)
    return sum([j + index + (50000000000 - 130) for j in range(len(grid)) if grid[j] == "#"])


if __name__ == '__main__':
    input_file_name = "input.txt"
    input_list = [x for x in read_input(input_file_name)]
    initial_state_grid = input_list[0].split(" ")[2].strip("\n")
    input_notes = {x.split(" ")[0]: x.split(" ")[2].strip("\n")for x in input_list[2:len(input_list)]}
    print(f'result part 1 = {part1(initial_state_grid, input_notes)}')
    print(f'result part 2 = {part2(initial_state_grid, input_notes)}')
