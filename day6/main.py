import numpy as np
from collections import Counter


def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def calculate_manhattan_distance(x1: int, y1: int, x2: int, y2: int) -> int:
    return abs(x2-x1)+abs(y2-y1)


def part1(l: list) -> int:
    #size = 10
    size = 500
    #max x = 353, max y =358 => grid of 500,500
    test = np.zeros(shape=(size, size), dtype=int)
    for i in range(size):
        for j in range(size):
            manhattan_distance = list(calculate_manhattan_distance(e[1], e[0], i, j) for e in l)
            test[i, j] = 99 if manhattan_distance.count(min(manhattan_distance)) > 1 else manhattan_distance.index(min(manhattan_distance))

    #find id of infinite area
    infinite_area_id = {x for x in range(size) if x in test[0, :] or x in test[:, 0] or x in test[-1, :] or x in test[:, -1]}
    infinite_area_id.add(99)

    cnt = Counter(z for z in test.flat if z not in infinite_area_id)
    return cnt.most_common(1)[0][1]


def part2(l: list) -> int:
    result = 0
    for i in range(500):
        for j in range(500):
            if sum(list(calculate_manhattan_distance(e[1], e[0], i, j) for e in l)) < 10000:
                result += 1

    return result


if __name__ == '__main__':
    input_file_name = "input.txt"
    #input_aurelien = 3293
    input_list = list([int(x.split(",")[0]), int(x.split(",")[1])] for x in read_input(input_file_name))
    print(f'result part 1 = {part1(input_list)}')
    print(f'result part 2 = {part2(input_list)}')
