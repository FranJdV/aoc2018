def read_input(f: str) -> str:
    with open(f, "r") as input_file:
        return input_file.readline()


def part1(polymer: list) -> int:
    old_length = 0
    while old_length != len(polymer):
        old_length = len(polymer)
        i = 0
        while i < len(polymer)-1:
            if abs(ord(polymer[i]) - ord(polymer[i+1])) == 32:
                polymer.pop(i)
                polymer.pop(i)
            else:
                i += 1
    return len(polymer)


def part2(polymer: str) ->int:
    return min(list(part1(list(polymer.replace(chr(97+i), "").replace(chr(65+i), ""))) for i in range(26)))


if __name__ == '__main__':
    input_file_name = "input.txt"
    input_str = read_input(input_file_name).strip("\n")
    print(f'result part 1 = {part1(list(input_str))}')
    print(f'result part 2 = {part2(input_str)}')
