import numpy as np


def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def read_claims(str_claims: list) -> list:
    claims = list()
    for claim in str_claims:
        c = claim.split(" ")
        claims.append([int(c[2].split(",")[0]), int(c[2].split(",")[1].strip(":")), int(c[3].split("x")[0]), int(c[3].split("x")[1])])
    return claims


def part1(str_claims: list) -> int:
    claims = read_claims(str_claims)
    test = np.zeros(shape=(1000, 1000), dtype=int)
    for c in claims:
        test[c[0]:c[0]+c[2], c[1]:c[1]+c[3]] += 1
    return (test >= 2).sum()


def part2(str_claims: list) -> int:
    claims = read_claims(str_claims)
    test = np.zeros(shape=(1000, 1000), dtype=int)
    for c in claims:
        test[c[0]:c[0]+c[2], c[1]:c[1]+c[3]] += 1

    for c in claims:
        if(test[c[0]:c[0]+c[2], c[1]:c[1]+c[3]] == 1).all():
            return claims.index(c)+1


if __name__ == '__main__':
    input_file_name = "input.txt"
    list_claim = list(x.strip("\n") for x in read_input(input_file_name))
    print(f'result part 1 = {part1(list_claim)}')
    print(f'result part 2 = {part2(list_claim)}')
