import re
from collections import deque


def read_input(f: str) -> str:
    with open(f, "r") as input_file:
        return input_file.readline()


def play(nb_players: int, last_marble: int) ->int:
    game = deque([0])
    player = 0
    score = [0] * nb_players
    for i in range(1, last_marble+1):
        if i % 23 != 0:
            game.rotate(-1)
            game.append(i)
        else:
            game.rotate(8)
            t = game.popleft()
            game.rotate(-1)
            score[player] = score[player] + i + t
        player = player + 1 if player + 1 < nb_players else 0
    return max(score)


def part1(nb_players: int, last_marble: int) -> int:
    return play(nb_players, last_marble)


def part2(nb_players: int, last_marble: int) -> int:
    return play(nb_players, last_marble*100)


if __name__ == '__main__':
    input_file_name = "input.txt"
    pattern = re.compile('([0-9]*) players; last marble is worth ([0-9]*) points')
    test = pattern.match(read_input(input_file_name)).groups()
    print(f'result part 1 = {part1(int(test[0]), int(test[1]))}')
    print(f'result part 2 = {part2(int(test[0]), int(test[1]))}')
