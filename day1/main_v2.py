import itertools


def read_input(f: str) -> list:
    with open(f, "r") as input_file:
        return input_file.readlines()


def part1(values: list) -> int:
    return sum(int(i) for i in values)


def part2(values: list) -> int:
    freq = 0
    set_freq = set()
    for i in itertools.cycle(values):
        freq += int(i)
        if freq in set_freq:
            return freq
        set_freq.add(freq)


if __name__ == '__main__':
    input_file_name = "input.txt"
    list_value = read_input(input_file_name)

    print(f'result part 1 = {part1(list_value)}')
    print(f'result part 2 = {part2(list_value)}')
