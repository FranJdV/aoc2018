
def read_input(f: str) -> list:
    input_list = []
    with open(f, "r") as input_file:
        for line in input_file:
            input_list.append(line)

    return input_list


def main():
    input_file_name = "input.txt"
    list_value = read_input(input_file_name)
    freq = 0
    set_freq = set()

#   first part :
#    for i in list_value:
#        freq+= int(i)
#    print("result = " + str(freq))

    freq_twice = False
    i = 0
    while not freq_twice:
        freq += int(list_value[i])
        if freq in set_freq:
            freq_twice = True
        set_freq.add(freq)
        i = 0 if i+1 == len(list_value) else i+1

    print("result = " + str(freq))


main()
